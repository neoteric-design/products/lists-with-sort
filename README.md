# lists-with-sort

## Introduction
This module will produce a listing of pages from a headless bundle provided. 

## Setup
Add the module to your `config.toml`:

```
[[module.imports]]
path = "gitlab.com/neoteric-design/products/lists-with-sort"
```

Ensure Hugo outputs the necessary JSON by adding the following lines to your `config.toml` as well:

```
[outputs]
section = ["HTML", "JSON"]
```

After adding the module to your project and running `hugo mod get`, add the JS to your `config.toml`:

`custom_js = ["js/sortedLists.js"]`

## Features
- Editors have the option of arbitrarily sorting the list in Forestry. This will be the default sort
- List can be sorted by:
  - Most recent dates
  - State
  - Part time or Full Time (this sorting feature will become more generic in the future) 
- Editors can set an expiration date for any listing

## List displays:
  - Organization name
  - Optional job length (full time, part time, or both)
  - title (in the case of UWM, this is job title)
  - City
  - State
  - Publish date
  - Button link and label

## Implementation
Insert `lists-with-sort` shortcode on any page, along with necessary params

## Example
[UW Shelter Medicine](https://60abb041154f440007b3d4e5--uwsheltermedicine.netlify.app/style-guides/list-with-filters/)

Ensure to copy the necessary Forestry files from the project example above, and add them to the root of your project

window.addEventListener("DOMContentLoaded", () => {
  if (document.contains(document.getElementById("list"))){
    initItemsSearch()
    
  let itemsData = {};
  let list = document.getElementById("list")
  let allItems = Array.from(list.getElementsByClassName("list-item"))
  
  
  function fetchItemsData() {
    const itemsUrl = "/about/index.json"
  
    return fetch(itemsUrl)
    .then((response) => {
      if (!response.ok) {
        throw new Error('Error fetching items data')
      }
      return response.json()
    })
    .catch((error) => {
      console.error("Could not set up items search\n", error)
    })
  }
  
  function filterItemsList(ids) {
    let nonmatches = []
  
    if(ids.length >= 0) {
      nonmatches = allItems.filter((item) => { return !ids.includes(item.id) })
    }
  
    allItems.forEach(showElement)
  
    nonmatches.forEach(hideElement)
  
  }
  
    function hideElement(element) {
      element.classList.add('hidden')
    }
  
    function showElement(element) {
      element.classList.remove('hidden')
    }
  
    // FILTER FUNCTIONS FOR EACH BUTTON

    function filterByLength(job_length) {
      let term = job_length.split("-", 1)
      filterItemsList(
        itemsData.
          filter((item) => { return item.job_length.includes(term)  }).
          map((item) => {
            if(item.job_length.includes(term)){
              return item.id
            } 
          })
      )
    }
  

    function filterByStates(stateAbbrevs) {
      filterItemsList(
        itemsData.
          filter((item) => { return stateAbbrevs.includes(item.state) }).
          map((item) => { return item.id })
      )
    }
  
    function initItemsSearch() {
      fetchItemsData()
        .then(data => {
          itemsData = data;
          return itemsData;
        })     
    }
  
  
  
    //TIE FUNCTIONS TO EVENTS
    let lengthBtns = document.querySelectorAll("button[name='filter']");
  
    lengthBtns.forEach(function(btns){
      btns.addEventListener("click", function(itemsData){
        filterByLength(this.id) 
        return itemsData;
      })
    })
  
    let stateFilter = document.querySelector("#location")
  
      stateFilter.addEventListener("change", function(itemsData){
        var selectedValue = stateFilter.options[stateFilter.selectedIndex].value;
          filterByStates(selectedValue)  
        return itemsData;
      })
  
  

    let dateBtn = document.querySelector("#dateBtn");
    // let originalList = Array.prototype.slice.call(document.querySelectorAll('.list-item'));
  
    dateBtn.addEventListener("click", function(itemsData){
     
      //init empty array
      // let sortedIDs = []

      //sort itemsData by date
      // filterByDate()
      allItems.forEach(showElement)
    })
  }
})
  